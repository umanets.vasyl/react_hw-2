import React, {Component} from 'react';
import './App.scss';
import Header from './components/Header/Header';
import Products from './components/Products/Products';

class App extends Component{

    state = {
      modal1:false,
      modal2:false,
      cart: localStorage.getItem('cart')  ? JSON.parse(localStorage.getItem('cart')) : [],
      favourite: localStorage.getItem('fav') ? JSON.parse(localStorage.getItem('fav')) : []
    }
    
 
  
  addToCart=(id)=>{
    this.setState({
      cart: [...this.state.cart,id]
    })
    localStorage.setItem('cart', JSON.stringify([...this.state.favourite, id]))
  }

  removeFromCart =(id)=>{
    this.state({
      ...this.state,
      cart: this.state.cart.filter((cartItem)=> cartItem !== id)
    })
    localStorage.setItem('cart', JSON.stringify(this.state.cart.filter((cartItem)=> cartItem !== id)))
  }
  addToFavourite = (id) => {
    this.setState({
      ...this.state,
      favourite: [...this.state.favourite, id]
    })
    localStorage.setItem('fav', JSON.stringify([...this.state.favourite, id]))
}
removeFromFavourite = (id) => {
  this.setState({
    ...this.state,
    favourite: this.state.favourite.filter((fav) => fav !== id)
  })
  localStorage.setItem('fav', JSON.stringify(this.state.favourite.filter((fav) => fav !== id)))
  

}
  render(){    
    return(
      <div className="App">
          <Header cartCount={this.state.cart.length} favCount={this.state.favourite.length}/>
          <Products addToCart={this.addToCart} 
                    removeFromCart={this.removeFromCart} 
                    cart ={this.state.cart} 
                    favourite={this.state.favourite}
                    addToFavourite = {this.addToFavourite}
                    removeFromFavourite = {this.removeFromFavourite}
                  />
    </div>
    )
  }
}
export default App;


