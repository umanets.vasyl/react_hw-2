import React, {Component} from "react";
import Button from "../Button/Button";
import '../Modal/Modal.scss'
class Modal extends Component{
    
    render(){
        const {header,text,btnClose,actions} = this.props
        return(
            <div className="modal__wrapper" onClick={btnClose}>
                
                <div className="modal__wrap">
                     <div className="modal__header">
                        <h4 className="modal__header-title">{header}</h4>
                        <Button text='X' className='button_close' onClick={btnClose}/>
                    </div>
                <div className="modal__main">
                    <p className="modal__main-text">{text}</p>
                </div>
                <div className="modal__footer">
                    {actions}
                </div>
                </div>
            </div>
        
        )
        
    }
}
export default Modal