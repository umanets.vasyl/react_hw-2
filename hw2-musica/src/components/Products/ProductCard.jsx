import React, { Component } from "react";
import "./ProductCard.scss";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";

class ProductCard extends Component {
  state = {
    modalCart: false,
    fav: false,
  };

  showCartModal = () => {
    this.setState({
      modalCart: true,
    });
  };
  closeCartModal = () => {
    this.setState({
      modalCart: false,
    });
  };

  addToFavourite = () => {
    if (this.props.isCardInFavourite) {
      this.props.removeFromFavourite(this.props.product.id);
    } else {
      this.props.addToFavourite(this.props.product.id);
    }
  };
  addToCart = () => {
    if (this.props.isCardInCart) {
      this.props.removeFromCart(this.props.product.id);
    } else {
      this.props.addToCart(this.props.product.id);
    }
  };
  render() {
    const { articul, img_url, name, color, price } = this.props.product;

    const actionsModalCart = (
      <div className="modal__footer-actions">
        <Button
          text="add"
          onClick={this.addToCart}
          className="button__modal-cart--footer"
        />
        <Button
          text="cancel"
          onClick={this.closeCartModal}
          className="button__modal-cart--footer"
        />
      </div>
    );

    return (
      <div className="card">
        <div className="card__header">
          <div
            className="favourite__icon-wrapper"
            onClick={this.addToFavourite}
          >
            <img
              className="favourite__icon"
              src={
                this.props.isCardInFavourite
                  ? "/img/icons/star-active.png"
                  : "/img/icons/star.png"
              }
              alt="star"
            />
          </div>
          <img className="card__header-img" src={img_url} alt="img" />
        </div>
        <div className="card__content">
          <div className="card__content-articul">Артикул: {articul}</div>
          <div className="card__content-title">Телевізор {name}</div>
          <div className="card__content-color">Колір: {color}</div>
        </div>
        <div className="card__footer">
          <div className="card__footer-price">{price}грн</div>
          <Button
            className="card__footer-button"
            onClick={this.showCartModal}
            text="add to cart"
          />
          {this.state.modalCart && (
            <Modal
              header="Do you want to add this product to the cart?"
              text="This product will be added to the cart!"
              btnClose={this.closeCartModal}
              actions={actionsModalCart}
            />
          )}
        </div>
      </div>
    );
  }
}
export default ProductCard;
