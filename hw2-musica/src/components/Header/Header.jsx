import React, {Component} from "react";
import '../Header/Header.scss'
import Cart from "./Cart";
import Favourite from "./Favourite";

class Header extends Component{
    render(){
        const {cartCount,favCount} = this.props
        return(
            <header className="header">
                <div className="header-wrap">
                    <div className="header-right">
                        <Favourite favCount={favCount}/>
                        <Cart cartCount={cartCount}/>
                    </div>
                </div>
            </header>
            
        )
    }
}

export default Header