import React,{Component} from "react";
import './Cart.scss'
class Cart extends Component{

    
    render(){
        const {cartCount} = this.props
        return (
          <div className="cart">
            <img
              className="cart__icon"
              src="./img/icons/red-shopping-cart-10906.png"
              alt="cart"
            />
            <p className="cart__count">({cartCount})</p>
          </div>
        );
    }
}
export default Cart;