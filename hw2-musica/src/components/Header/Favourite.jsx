import React,{Component} from "react";
import './Favourite.scss'
class Favourite extends Component{
    
    render(){
        const {favCount} = this.props
        return (
          <div className="favourite__wrap">
            <img
              className="favourite__wrap-img"
              src="/img/icons/star-active.png"
              alt="star"
            />
            <p className="favourite__count">({favCount})</p>
          </div>
        );
    }
}
export default Favourite;